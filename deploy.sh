#!/bin/bash

set -e
set -x

if [ -n "$FTP_USER" ] && [ -n "$FTP_PASSWORD" ] && [ -n "$FTP_HOST" ]; then
  echo "Upload files over FTP"
  lftp -e "set log:file/xfer; mirror --parallel=10 --delete --reverse --ignore-time deploy/ /elektriker-ohne-grenzen/; quit" --user $FTP_USER --password $FTP_PASSWORD $FTP_HOST
fi
