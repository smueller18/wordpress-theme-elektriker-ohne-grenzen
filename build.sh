#!/bin/bash

set -e
set -x

rm -rf deploy/
mkdir -p deploy

cp -r $(ls -A | grep -vE "deploy|.git|.gitignore|.gitlab-ci.yml|.env|deploy.sh|build.sh") deploy/

for file in `find deploy -name "*.po"`; do
  msgfmt -o ${file/.po/.mo} $file
done

for file in $(find deploy/media -type f -name "*.css" ! -name "*.min.css"); do
  output_file=$(echo $file | sed 's/\.css/\.min\.css/g')
  cleancss -o $output_file $file
  rm -f $file
done
