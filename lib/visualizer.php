<?php

add_action( 'rest_api_init', 'create_api_visualizer_chart_content' );

function create_api_visualizer_chart_content() {
  register_rest_field( 'visualizer', 'chart_content', array(
    'get_callback' => function( $post ) {
        return unserialize(get_post($post['id'])->post_content);
    },
    'update_callback' => function( $data, $post ) {
      wp_update_post(array(
        'ID' => $post->ID,
        'post_content' => serialize($data),
      ));
      # When opening visualizer, it restores old revisions. Delete all known revisions to prevent restoring.
      foreach(wp_get_post_revisions($post->ID) as $id => $revision) {
        wp_delete_post_revision($id);
      };
    },
    'schema' => array(
      'items' => array(
        'type' => 'array',
        'items' => array(
          'type' => 'array',
          'items' => array(
            'type' => 'string'
          )
        )
      )
    )
  ));
}

?>
