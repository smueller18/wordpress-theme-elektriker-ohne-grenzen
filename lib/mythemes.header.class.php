<?php
if( !class_exists( 'mythemes_header' ) ){

class mythemes_header
{
	static function setup()
	{
		$args = array(
            'default-image'          => '',
            'random-default'         => true,
            'width'                  => 2560,
            'height'                 => 1440,
            'flex-height'            => true,
            'flex-width'             => true,
            'default-text-color'     => 'ffffff',
            'header-text'            => true,
            'uploads'                => true
        );

        add_theme_support( 'custom-header', $args );
	}

	static function head()
	{
        get_template_part( 'templates/head' );
		get_template_part( 'templates/style' );
	}
}

}	/* END IF CLASS EXISTS */
?>
