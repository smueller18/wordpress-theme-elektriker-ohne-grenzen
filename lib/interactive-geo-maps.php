<?php

add_action( 'rest_api_init', 'create_api_igmap_map_info_round_markers' );

function create_api_igmap_map_info_round_markers() {
  register_rest_field( 'igmap', 'map_info_round_markers', array(
    'get_callback' => function( $post ) {
        $mapInfo = get_post_meta($post['id'], 'map_info', true);
        return $mapInfo['roundMarkers'];
    },
    'update_callback' => function( $data, $post ) {
      $mapInfo = get_post_meta($post->ID, 'map_info', true);
      $mapInfo['roundMarkers'] = $data;
      update_post_meta($post->ID, 'map_info', $mapInfo);
    },
    'schema' => array(
      'items' => array(
        'type' => 'array',
        'items' => array(
          'type'       => 'object',
          'properties' => array(
            'id' => array(
              'type' => 'string',
            ),
            'coordinates' => array(
              'type' => 'object',
              'properties' => array(
                'address' => array(
                  'type' => 'string',
                ),
                'latitude' => array(
                  'type' => 'string'
                ),
                'longitude' => array(
                  'type' => 'string'
                ),
                'zoom' => array(
                  'type' => 'string'
                ),
              )
            ),
            'tooltipContent' => array(
              'type' => 'string',
            ),
            'content' => array(
              'type' => 'string',
            ),
            'useDefaults' => array(
              'type' => 'string',
            ),
            'radius' => array(
              'type' => 'string',
            ),
            'fill' => array(
              'type' => 'string',
            ),
            'hover' => array(
              'type' => 'string',
            ),
            'action' => array(
              'type' => 'string',
            ),
            'value' => array(
              'type' => 'string',
            ),
          )
        )
      ),
    ),
  ));
}

?>
