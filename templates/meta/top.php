<?php
    $show_top_meta = is_singular( 'post' ) ? (bool)get_theme_mod( 'mythemes-top-meta', true ) : true;

    if( $show_top_meta ){
?>
        <div class="mythemes-top-meta meta">

            <!-- GET FIRST 2 CATEGORIES -->
            <?php the_category(); ?>

            <!-- DATE -->
            <?php
                $t_time = get_post_time( 'Y-m-d', false , $post -> ID  );
                $u_time = date_i18n ( esc_attr( get_option( 'date_format' ) ), get_post_time( ) );
            ?>
            <time datetime="<?php echo esc_attr( $t_time ); ?>"><?php echo sprintf( __( 'on %s' , 'cannyon' ), $u_time, false, $post -> ID, true ); ?></time>

            <!-- COMMENTS -->
            <?php
                if( $post -> comment_status == 'open' ) {
                    $nr = absint( get_comments_number( $post -> ID ) );
                    echo '<a class="comments" href="' . esc_url( get_comments_link( $post -> ID ) ) . '">';
                    echo '<span>' . sprintf( _nx( '%s Comment' , '%s Comments' , absint( $nr ) , 'Number of comment(s) from post meta' , 'cannyon' ) , number_format_i18n( $nr ) ) . '</span>';
                    echo '</a>';
                }
            ?>
        </div>
<?php
    }
?>
